!
[](/assets/234_deuxvisions.jpg)
[Caroline Delieutraz, Deux visons](http://www.delieutraz.net/fr/deux-visions/)

# GeoNetArt

### Présentation du travail de fin d'année

* Briefing sur digitalab.be: [http://www.digitalab.be/briefing-geonetart-2018/](http://www.digitalab.be/briefing-geonetart-2018/)
* Dates de lectures: 
* Remises finales: 
* Valeur du travail: 30% des points du quadrimestre.

Envisager le lieu en tant que récit, mémoire, structure, mouvement, interaction..
Concevoir un écho sur le web en prenant en compte les caractéristiques de ce site en ligne \(connectivité au réseau, liens hypertextes, temps réel, diversité des périphériques..\)

N'oubliez pas que ce que permet HTML aujourd'hui va bien au delà de ce qui a été vu au cours\(video, son, animation css..\) sans que cela soit forcément plus compliqué.

Lors de la première lecture du travail :

* Le lieu de référence doit être choisi
* La façon doit le site en ligne fera référence ou dialoguera avec le lieu doit déjà avoir été envisagée.
* Un projet de conception de site doit être réalisé \(même si c'est sur une feuille de papier\)
* Ces différents points seront évalués.

# CSS pour les mobiles

* Media Queries
* Quelques règles de base ([Un exemple ici](https://www.alsacreations.com/astuce/lire/1177-Une-feuille-de-styles-de-base-pour-le-Web-mobile.html))

```css
body {
    font-size:14px;
}
.menu {
    display:flex;
}
img {
    max-width:100%;
}

@media screen and (max-width: 767px) {
    body {
        font-size:16px;
    }
    .menu {
        flex-direction:column;
    }
}
```

# Liens

* [https://fr.qr-code-generator.com/](https://fr.qr-code-generator.com/)

# Wordpress

### Introduction et installation

* .org, .com, wix, etc.
* Principe \(Bases de données, PHP, MySQL..\)
* **Installation** + One click install
* Extensions, thèmes
* Le HTML et CSS dans Wordpress

### Liens utiles

* Télécharger Wordpress
* Thèmes
* Architecture d'un thème Wordpress

### Installation de WP: les étapes

* Télécharger WP \(.org !\), en français par ex.
* Créer une base de données \(notez les user et password + serveur de BDD\)
* Créer un dossier wp à la racine de votre serveur, par exemple.
* Dézippez l'archive WP que vous avez téléchargée
* Glissez son contenu dans le dossier wp que vous venez de créer sur votre serveur
* Ensuite, accédez à monsite.com/wp dans votre navigateur préféré
* Configurez
* Modifiez le background de la homepage




