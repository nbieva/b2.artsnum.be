###Pour rappel, lors de la première lecture du travail :

* Le lieu de référence doit être choisi
* La façon doit le site en ligne fera référence ou dialoguera avec le lieu doit déjà avoir été envisagée.
* Un projet de conception de site doit être réalisé \(même si c'est sur une feuille de papier\)
* Ces différents point seront évalués.
* Soyez là au moins 5 minutes avant l'heure prévue


### Wordpress dans le détail

* Types de contenus \(articles, pages, medias..\) et Custom posts types
* Menus
* Extensions
* Widgets
* Thèmes
+ Installer un thème > [https://fr-be.wordpress.org/themes/onepress/](https://fr-be.wordpress.org/themes/onepress/)
+ Où se situent les thèmes?
+ Architecture de Wordpress sur le serveur

###Ce qui est spécifique aux thèmes
+ Les **emplacements de menus**
+ Les **types de contenus** supplémentaires
+ Vérifier le **menu** admin + les options et réglages.
+ A vérifier avant d'installer: Dernière mise à jour, popularité, cotes et commentaires.

###Les extensions (plugins)
+ A vérifier avant d'installer: Dernière mise à jour, popularité, cotes et commentaires.
+ GUTENBERG

###Ce que nous avons fait:
+ Téléchargement et installation de Wordpress sur votre serveur
+ Articles(+ catégories), pages et medias
+ Menus, widgets

###Cette semaine:
+ Retour sur les articles, pages et catégories
+ Images à la une dans les articles
+ Thèmes
+ Customisation CSS (ou html)
+ Les extensions
+ Créer un thème Wordpress??

###Au cours:
+ Site en ligne
+ **Télécharger et installer le thème [OnePress](https://fr-be.wordpress.org/themes/onepress/)**
+ Modifier l'aspect des boutons (+ survol)
+ Masquer les commentaires à l'aide de CSS
+ Ajouter un titre au dessus de la sidebar (Sidebar)
+ Installer l'extension Gutenberg + tester
+ **Créer 5 articles, 2 catégories et une page**
+ **Créer un menu de navigation**
+ **Ajouter minimum quatre éléments de menu, dont un pointe vers l'extérieur (Google.com par ex.), un vers votre page et un autre vers une de vos catégories**
+ Ajouter une vidéo (Youtube, Dailymotion, Vimeo...) dans un de vos articles
+ **Ajouter/personnaliser 3 widgets**
+ Chercher un autre thème
+ **Me communiquer l'URL de votre site
**

