Testing/présences

## Aujourd'hui

+ Supprimer le lien webhost sur nos pages
+ Création d'une base de données chez l'hébergeur
+ Présentation et installation de Wordpress

## Peut-être...

* P5js
* https://flawlessapp.io/designtools
* Fancybox et les images
* Frameworks CSS

Serveur local avec [MAMP](https://www.mamp.info/en/) ou [WAMP](http://www.wampserver.com/)
+ Démo installation de Wordpress (en local sur mon ordinateur)
* Bases de données et configuration d'un hébergement

## Exercice au cours:

* Créer une galerie photo en s'aidant de la documentation de Fancybox