#
#Ressources utiles:

+ http://curlybraces.be/wiki/Ressources::CSS
+ https://learn.shayhowe.com/html-css/opening-the-box-model/
+ http://thecodeplayer.com/walkthrough/animated-gradient-effect

##Aujourd'hui

+ Retour sur le dernier cours (Classes et identifiants HTML)
> Classes et identifiants HTML, La cascade, principes de base
+ Le [modèle de boîte](https://learn.shayhowe.com/html-css/opening-the-box-model/) (box-sizing:border-box;)
+ Les unités (px, %, em, rem, vh, vw, cm..)
+ Les propriétés du jour
+ Google Web Fonts

##Pour le prochain cours (6e exercice):
+ Refaites le design de la page d'accueil à l'aide de CSS et des propriétés vues au cours, entre autres (voir ci-dessous). Pour les backgrounds: au moins une image d'arrière-plan, qui change quand on survole l'élément.
+ Chargez et utilisez une nouvelle police Google Fonts
+ Mettez votre site à jour

###Liens:
+ https://css-tricks.com/almanac/properties/b/background-size/

##Les propriétés CSS du jour
+ background
+ background-image
+ background-repeat
+ background-size
+ background-position
+ backgroud-color
+ font-size
+ font-weight
+ font-family
+ width
+ height
+ margin
+ padding
+ border
+ :hover

![](https://camo.githubusercontent.com/7a68ce3b3f3e4552739934c6978874139d7ff4ff/68747470733a2f2f6373732d747269636b732e636f6d2f77702d636f6e74656e742f636c617373706c757369642e706e67)

![](/assets/block.png)

![](https://github.com/lam-artsnum/b2/wiki/images/albers.png)
