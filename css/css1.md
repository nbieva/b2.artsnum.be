![](/assets/zen1.png)

##Aujourd'hui
+ Formulaire pour ceux qui ne l'ont pas fait.
+ Présences
+ Principes de base de CSS > http://curlybraces.be/wiki/Ressources::CSS
+ Classes et identifiants HTML
+ La cascade
+ Le modèle de boîte (box-sizing:border-box;)
+ Anatomie d'une règle CSS - [Propriétés](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3/memento-des-proprietes-css)
+ Les sélecteurs
+ Lier une feuille de style à un fichier HTML
+ La couleur dans les CSS


```css
/* On cible les paragraphes (P) */
p {
color:#333;
}

/* On cible les éléments SPAN qui sont dans un paragraphe */
p span {
color:red;
}

/* On cible les paragraphes avec la classe INTRODUCTION */
p.introduction {
color:blue;
}

/* On cible les éléments (quels qu'ils soient) avec la classe INTRODUCTION */
.introduction {
/* propriétés */
}
/* On cible les éléments SPAN qui sont dans un élément(quel qu'il soit) avec la classe INTRODUCTION */

.introduction span {
/* propriétés */
}

/* On cible les liens qui sont dans des LI, qui sont dans des UL (c'est généralement le cas) */
ul li a {
/* propriétés */
}

/* On cible les liens survolés qui sont dans des LI, qui sont dans des UL */
ul li a:hover {
/* propriétés */
}

img {
opacity:0.9;
}

img:hover {
opacity:1;
}

.legende {
display:none;
}
```

