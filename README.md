
#Horaires des remises finales du 13 juin

Vous trouverez ci-dessous les horaires de passage des remises finales qui auront lieu à l'Abbaye, Salle art numérique 1 ou 2.
Veillez à relire attentivement le briefing du travail sur digitalab.be, tester vos sites en ligne et avoir avec vous les fichiers de votre travail lors de la remise.

briefing: http://www.digitalab.be/briefing-geonetart/

Dans tous les cas, soyez-là 5 minutes avant l'heure prévue.

##Remises finales : jeu 13 juin

+ **Manon** 09:00
+ **Lara** 09:13
+ **Elise** 09:26
+ **Amarilda** 09:39
+ **Livia** 09:52
+ **Emma** 10:05
+ **Lucie** 10:18
+ **Leonor** 10:40
+ **Iskandar** 10:50
+ **Thil** 11:03
+ **Donghyun** 11:16
+ **Cho H.** 11:29
+ **Maj-Britt** 11:42
+ **Mounir** 11:55
+ **Salomé** 12:08
+ **Coralie** 12:21
+ **Francesco**	 13:20
+ **Gabriela**	13:33
+ **Lou Anh** 13:46	
+ **Nina** 13:59	
+ **Octave** 14:12	
+ **Juliette**	14:40
+ **Luna-Louise** 14:53
+ **Nathalie** 15:09	
+ **Maud** 15:22	
+ **Agathe** 15:35
+ **Kamand** 15:48	

**IMPORTANT :** Les étudiants non présents dans cette liste et désirant passer les évaluations de juin pour ce cours peuvent me contacter directement par mail.