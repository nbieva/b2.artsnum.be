# Summary

## WEB B2

* [Transfert FTP](general/ftp.md)
* [Exercices](general/exercices.md)
* [Remises de fin d'annnée](general/findannee.md)
  
## Partie 1
* [Intro générale + HTML](html/html1.md)
* [Architecture d'un site](html/html2.md)
* [CSS & classes](html/html3.md)
* [Atelier CSS](html/html4.md)
* [Cours 6](css/css1.md)
* [Cours 7](css/css2.md)
* [Cours 8](general/cours8.md)
* [Cours 9](css/css4.md)

##Wordpress
* [Cours 10](wordpress/wordpress1.md)
* [Cours 11](wordpress/wordpress2.md)

##Codes
* [Ultra basic HTML](code/ultra-basic-html.md)
* [Ultra basic CSS](code/css-ex-recap-html.md)
* [CSS Zen Garden](code/css-zen-garden.md)
* [HTML avec classes](code/html-avec-classes.md)
* [AlberCSS](code/albers.md)
* [Flexbox](code/flexbox.md)