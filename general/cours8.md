## Au programme aujourd'hui

* Transfert FTP
* (Et Bootstrap pour le cours du vendredi)

## Pour le prochain cours \(après le congé\)

**A faire:** 

* Inscription sur webhost et dépôt d'un fichier html accessible (m'envoyer l'adresse)

**Voici un petit résumé, ou mémo, sur ce qui a été vu jusque maintenant:**

+ Créer une **page simple HTML** avec quelques éléments de base (div, span, titres, paragraphes, listes à puces, images, etc..)
+ Comprendre la syntaxe et la structure d'un document HTML
+ Savoir créer l'**architecture d'un site** (organisation des fichiers et dossiers)sur son ordinateur par exemple.
+ Savoir naviguer dans cette architecture (remonter d'un dossier, adresses des **liens, relatifs et absolus**, etc..)
+ Savoir mettre ce contenu en ligne, via **Netlify**. 
+ Lier une **feuille de style CSS** externe à un fichier HTML.
+ Comprendre la syntaxe d'une règle CSS
+ Comprendre ce que sont les **classes** (attributs HTML).
+ Savoir les utiliser dans CSS (savoir comment cibler des éléments de votre fichier html à l'aide de CSS)
+ Comprendre les types **BLOCK** et **INLINE** d'éléments HTML
+ Comprendre comment les modifier via la propriété css **DISPLAY** (savoir ce que chaque valeur de la propriété implique.. Celles vues au cours du moins.)
+ Savoir aligner des éléments les uns dans les autres à l'aide de Flexbox et des quelques propriétés vues au cours.
+ Savoir ce que sont les unités px, %, vw et vh.
+ Savoir utiliser les sélecteurs CSS repris dans la liste ci-dessous.
+ Donner une image d'arrière plan à une page web
+ Charger une (ou plusieurs) nouvelles polices de caractères pour votre site web via **Google Fonts**, et les utiliser dans vos CSS.
+ Savoir, bien entendu, mettre son site à jour

**Liste de propriété CSS utiles, abordées au cours (faites une recherche sur le web si nécessaire...). En vrac:**

+ display
+ width
+ height
+ padding
+ margin
+ border
+ background
+ color
+ border-radius
+ text-decoration
+ text-transform
+ opacity
+ justify-content
+ align-items
+ flex-direction
+ etc.
+ ...

---

![](https://github.com/lam-artsnum/b2/wiki/images/albers.png)

