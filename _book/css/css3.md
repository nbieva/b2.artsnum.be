Testing / presences

# Au programme aujourd'hui

* **La propriété display \(block, inline, inline-block, flex, none ..\)**
* position \(static, relative, absolute, fixed ..\)
* float \(left, right, none ..\)
* box-sizing: border-box;
* **border**
* **gradients + générateurs CSS**
* cursor
* animation
* ...

# Pour vous aider avec les positionnements CSS

* [http://flexboxfroggy.com/\#fr](http://flexboxfroggy.com/#fr) \(Jusqu'au \#13 vous aidera déjà énormément!\)
* [http://fr.learnlayout.com/](http://fr.learnlayout.com/)

# Exercice au cours:

* Créez un menu de navigation horizontal \(en utilisant la propriété display\)
* Le rendre fixe lors du scroll

# Pour le prochain cours \(après le congé\)

* **A faire:** Prenez le temps de comprendre les 13 premières étapes de ce tutoriel sur Flexbox. Cela concerne principalement 3 propriétés \(justify-content, align-items, flex-direction\) &gt; [http://flexboxfroggy.com/\#fr](http://flexboxfroggy.com/#fr) Rappelez-vous la fenêtre alignements dans Illustrator par exemple..

Il y a eu encore beaucoup d'absents au dernier cours malheureusement. Pour les personnes concernées: mettez-vous à jour dans la matière, via des tutoriels ou autres. Il y en a d'excellents sur le web, et pas mal de liens intéressants dans ces pages-ci aussi.

**Voici un petit résumé, ou mémo, sur ce qui a été vu jusque maintenant:**

+ Créer une **page simple HTML** avec quelques éléments de base (div, span, titres, paragraphes, listes à puces, images, etc..)
+ Comprendre la syntaxe et la structure d'un document HTML
+ Savoir créer l'**architecture d'un site** (organisation des fichiers et dossiers)sur son ordinateur par exemple.
+ Savoir naviguer dans cette architecture (remonter d'un dossier, adresses des **liens, relatifs et absolus**, etc..)
+ Savoir mettre en ligne, via **FTP**, ce contenu sur un serveur (souscrire à un hébergement, configurer FileZilla, transférer les fichiers..)
+ Lier une **feuille de style CSS** externe à un fichier HTML.
+ Comprendre la syntaxe d'une règle CSS
+ Comprendre ce que sont les **classes** (attributs HTML).
+ Savoir les utiliser dans CSS (savoir comment cibler des éléments de votre fichier html à l'aide de CSS)
+ Comprendre les types **BLOCK** et **INLINE** d'éléments HTML
+ Comprendre comment les modifier via la propriété css **DISPLAY** (savoir ce que chaque valeur de la propriété implique.. Celles vues au cours du moins.)
+ Créer un menu de navigation horizontal.
+ Savoir utiliser les sélecteurs CSS repris dans la liste ci-dessous.
+ Donner une image d'arrière plan à une page web
+ Charger une (ou plusieurs) nouvelles polices de caractères pour votre site web via **Google Fonts**, et les utiliser dans vos CSS.
+ Savoir, bien entendu, mettre son site à jour

**Liste de propriété CSS utiles, abordées au cours (faites une recherche sur le web si nécessaire...). En vrac:**

+ display
+ width
+ height
+ padding
+ margin
+ border
+ background
+ color
+ border-radius
+ text-decoration
+ text-transform
+ opacity
+ ...




---

![](https://github.com/lam-artsnum/b2/wiki/images/albers.png)

