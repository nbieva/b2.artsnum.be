![](https://media0.giphy.com/media/Kd8QOZ5WrjSmY/source.gif)

**Olia Lialina**, Summer, 2013
[http://kimasendorf.com/olia/summer/](http://kimasendorf.com/olia/summer/)

---

Nous avons vu au dernier cours ce qu'étaient serveur, client et client FTP, mais aussi la structure d'un document HTML.

> Quelles sont les principales parties d'un document HTML ?

Cette structure de document était également visible grâce à inspecteur web de firefox qui, il y a quelques temps encore, proposait une vue 3D de la structure de votre page \(ci-dessous\). Pour visualiser votre page en 3D, vous pouvez aujourd'hui ajouter [Tilt](https://addons.mozilla.org/fr/firefox/addon/tilt/#&gid=1&pid=5) à Firefox \(extension, ou Add-on\).

[![](http://blogzinet.free.fr/images/tilt_3d_mozilla_firefox11_originale.png)](https://addons.mozilla.org/fr/firefox/addon/tilt/#&gid=1&pid=5)

---

### Au programme

* Présences
* Réception des exercices
* Rappel: Serveurs, domaines, IP, ...
* Retour sur vos premières pages HTML. Correctifs.
* Netlify: Première tentative mise en ligne (+ou création de comptes W3 schools, CodePen ou jsFiddle)
* Retour sur le sens du HTML \(tel, address, map..\)
* Les commentaires et leur fonction
* [https://developer.mozilla.org/fr/Apprendre/HTML/Introduction\_à\_HTML/Getting\_started](https://developer.mozilla.org/fr/Apprendre/HTML/Introduction_à_HTML/Getting_started)
* [https://learn.shayhowe.com/html-css/building-your-first-web-page/](https://learn.shayhowe.com/html-css/building-your-first-web-page/)
* Articulation HTML/CSS/JS + Architecture d'un site > 
* [Liens, Chemins relatifs et absolus](https://www.w3schools.com/html/html_filepaths.asp). Ancres à l'intérieur d'un document.
* Modifiez la couleur de fond de votre page + 1 autre élément html (h1, p, a ...)
* Images et pages web \(RVB, résolution..\)
* h1, h2, h3, p, ul, li, a, img, strong, em, blockquote, hr, br


##### Les élements que nous verrons aujourd'hui:

```html
<a href="#mon_ancre">Aller à mon ancre</a>
    <div id="mon_ancre">
</div>
```

###Au cours:

Sur une thématique choisie, construction de **trois pages HTML** \(ou plus\), **liées entre elles** \(par un menu de navigation\) et reprenant les différents éléments suivants:

1. Plusieurs paragraphes
2. Plusieurs titres \(de différents niveaux\)
3. Un menu de navigation
4. Au moins une image
5. Un lien sur à image, pointant vers une URL extérieure et 'ouvrant dans un nouvel onglet.

Et pourquoi pas \(facultatif\):

1. Insert d'une video Youtube/Vimeo/Facebook...