



![](http://catalog.rhizome.org/images/c/c9/Ca-424.jpg)
**Paul Slocum**, [Time lapse Homepage](https://www.youtube.com/watch?v=nFiMtHciR4Y) (2005), video, 55'

+ Evan Roth, [All HTML Elements](http://all-html.net/), 2011
+ [JODI](http://wwwwwwwww.jodi.org/) + [GEO GOO](http://geogoo.net/)
+ Internet Archive, Wayback machine, Webrecorder

### Nous savons:

* Ce qu'est HTML
* Ce que sont navigateur, serveur, IP, nom de domaine..
* Ce qu'est un inspecteur web et ce qu'est un code source.
* Créer une page HTML simple
* Ce que sont les balises `<head>`, `<title>`, `<meta>`, `<body>` ainsi que toute une série de balises du corps de la page.. \(`<h1>`, `<h2>`, `<p>`, `<ul>`, `<li>`, `<a>`, `<img>`, etc. \)
* Créer plusieurs pages entre elles, interconnectées, avec un menu de navigation.
* Un peu de ce qu'il en est des URL de fichiers \(Chemins relatifs et absolus\) (voir aujourd'hui)
* Recherche d'images et copies d'adresses \(URLs\)

### Aujourd'hui

+ Chemins relatifs et absolus (rappel sur l'architecture d'un site web)
+ Les images: Résolution, tailles, formats pour le web
+ Liens vers une adresse email? (mailto: ) Vers un numéro de téléphone? (tel: )
+ Nouvelles balises : div, span, button... (W3C)
+ Introduction à CSS
+ [CSS Zen Garden](http://www.csszengarden.com/)
+ Feuilles de styles externes et styles inline.
+ La cascade
+ IDs et classes
+ Premières déclarations CSS
+ Modèle de boîte CSS
+ La propriété display
+ Couleurs (text, background, border)
+ :hover
+ Hacker les CSS d'un site en ligne via l'inspecteur
+ Styler un menu de navigation
+ Insertion d'une video Youtube/vimeo ou d'une map

Eventuellement [transfert FTP](/transfert-ftp.md) test@ testartsnum2018;
et Eléments de [formulaires](https://www.w3schools.com/html/html_form_elements.asp) : form, input, textarea, label, select, option, datalist

# Liens relatifs et absolus

Il existe 2 principales manière d'indiquer le chemin d'accès à un fichier \(HTML,img, PDF..\) à partir d'un autre. On parlera de chemins relatifs, ou absolu.

### Le chemin relatif

Le chemin relatif, l'est par rapport à cotre page HTML, par exemple. On indiquera le chemin A PARTIR DE votre fichier. Si la relation entre les deux change, le fichier sera "perdu".

```html
<a href="dossier/page.html">Ma page</a>
```
ou si on veut atteindre un fichier, dans un dossier placé à côté de notre page :
```html
<a href="../dossier/page.html">Ma page</a>
```
### Le chemin absolu

Le chemin absolu sera valide, même si vous déplacez votre page. Le chemin ci-dessous est un chemin absolu, mais à partir de la racine du serveur. (slash au début)
```html
<a href="/dossier/page.html">Cliquez ici</a>
```
C'est également ce que vous obtiendrez si vous faites un "clic-droit" + "Copier l'adresse de l'image" sur une image dans votre navigateur. Vous obtiendrez une URL de ce genre:

```html
<a href="https://static.fnac-static.com/multimedia/FR/Images_Produits/FR/fnac.com/Visual_Principal_340/4/1/4/9782070404414/tsp20121014065225/Le-chercheur-d-absolu.jpg">Cliquez ici</a>
```
Un chemin absolu peut parfois être bien plus simple que celui ci-dessus.

####MAIS!
Dans tous les cas, si un fichier ou un dossier est renommé, ou si une extension change \(même de .jpg à .JPG\), le chemin sera mort et le fichier non-trouvé.


![](/assets/liensabsolusetrelatifs.png)

#CSS

Vous trouverez ci-dessous le sélecteur universel définissant le modèle de boîte à utiliser pour tous vos éléments (à copier-coller dans le haut de votre feuille de styles).

```css
* {
box-sizing: border-box;
}
```

##Propriétés
+ color
+ background
+ padding
+ margin
+ border
+ Block & inline
+ ... (voir [cette liste](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3/memento-des-proprietes-css) ou [cette page](https://www.supinfo.com/articles/single/3865-principales-proprietes-langage-css-leurs-valeurs).


##Comment cibler un (ou des) élément(s) avec CSS

```css
/* On cible les paragraphes (P) */
p {
color:#333;
}

/* On cible les éléments SPAN qui sont dans un paragraphe */
p span {
color:red;
}

/* On cible les paragraphes avec la classe INTRODUCTION */
p.introduction {
color:blue;
}

/* On cible les éléments (quels qu'ils soient) avec la classe INTRODUCTION */
.introduction {
/* propriétés */
}
/* On cible les éléments SPAN qui sont dans un élément(quel qu'il soit) avec la classe INTRODUCTION */

.introduction span {
/* propriétés */
}

/* On cible les liens qui sont dans des LI, qui sont dans des UL (c'est généralement le cas) */
ul li a {
/* propriétés */
}

/* On cible les liens survolés qui sont dans des LI, qui sont dans des UL */
ul li a:hover {
/* propriétés */
}

img {
opacity:0.9;
}

img:hover {
opacity:1;
}

.legende {
display:none;
}
```