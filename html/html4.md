![](/assets/francine.png)

http://diana-adrianne.com/purecss-francine/

https://github.com/cyanharlow/purecss-francine

+ Retour sur CSS et sur les classes > http://curlybraces.be/wiki/Ressources::CSS
+ Les identifiants
+ Eléments de types block, inline-block et inline
+ Le modèle de boîte CSS
+ Les unités CSS (px, %, vw, vh, em, rem, ..)
+ Styler un menu de navigation

###Quelques mémos

+ http://mdl.nokto.net/memo-css3.pdf
+ https://www.pixelcrea.com/ressources/memo-css3.pdf
+ https://jenseign.com/html/wp-content/uploads/2018/01/memo-html-apprendre.pdf

Vous pouvez faire vos tests dans https://jsbin.com par exemple, avant de les rapatrier sur votre machine...

![](https://camo.githubusercontent.com/0cd8f5c7d5d1ff3644a7d3356674b897dd29836e/68747470733a2f2f73747579687364657369676e2e66696c65732e776f726470726573732e636f6d2f323031352f31302f626f782d6d6f64656c2e706e67)

![](/assets/albers2.png)


#Propriétés CSS utiles

+ cursor
+ interactivité
+ ... (voir [cette liste](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3/memento-des-proprietes-css) ou [cette page](https://www.supinfo.com/articles/single/3865-principales-proprietes-langage-css-leurs-valeurs).

###Sélecteur universel

```css
* {
box-sizing: border-box;
}
```

###Box model

+ display
+ padding
+ width
+ height
+ max-width, max-height
+ min-width, min-height
+ margin
+ border
+ border-radius

###[Le cas flexbox](https://flexboxfroggy.com/#fr)

+ display: **flex**; (ou **inline-flex**)
+ flex-direction: **row**; (ou column)
+ justify-content: **center**; (ou **start**, ou **end**, ou **space-between**, ou **space-aroud**)
+ align-items: **center**; (ou **start**, ou **end**)

###Color

+ color (pour le texte)
+ background (ou background-color)
+ linear-gradient : http://www.colorzilla.com/gradient-editor/

###Typographie

+ font-family
+ font-size
+ font-weight
+ font-style
+ text-decoration
+ text-transform
+ text-align
+ line-height
+ color

###Background

+ background
+ background-color
+ background-size
+ background-repeat
+ background-position

###Position

+ position
+ float
+ (flex)
+ z-index

###Animation

+ [transitions](https://daneden.github.io/animate.css/)

##Comment cibler un (ou des) élément(s) avec CSS

```css
/* On cible les paragraphes (P) */
p {
color:#333;
}

/* On cible les éléments SPAN qui sont dans un paragraphe */
p span {
color:red;
}

/* On cible les paragraphes avec la classe INTRODUCTION */
p.introduction {
color:blue;
}

/* On cible les éléments (quels qu'ils soient) avec la classe INTRODUCTION */
.introduction {
/* propriétés */
}
/* On cible les éléments SPAN qui sont dans un élément(quel qu'il soit) avec la classe INTRODUCTION */

.introduction span {
/* propriétés */
}

/* On cible les liens qui sont dans des LI, qui sont dans des UL (c'est généralement le cas) */
ul li a {
/* propriétés */
}

/* On cible les liens survolés qui sont dans des LI, qui sont dans des UL */
ul li a:hover {
/* propriétés */
}

img {
opacity:0.9;
}

img:hover {
opacity:1;
}

.legende {
display:none;
}
```

##Liens:

+ CyberDuck: https://cyberduck.io/
+ CSS Zen Garden: http://www.csszengarden.com/
+ ANIMATE: https://daneden.github.io/animate.css/
+ Brutalism: http://brutalistwebsites.com/