{{ 'https://vimeo.com/157337087' | video }}
(http://www.artdelicorp.com/category/hacking-the-browser/), Chrome extension, 2016

---

> The single most important thing you need in order to have a career in the arts is persistence. The second most important thing you need is talent. The third most important thing is a grounding in how the online world works. Its that Important.
>
> **Cory Doctorow**
> from [Information Doesn't Want to Be Free: Laws for the Internet Age.](https://store.mcsweeneys.net/products/information-doesn-t-want-to-be-free) 2014 \(in [Net art && cultures](http://netart.rocks/syllabus)\)

## Introduction

* [Présences, listes](https://docs.google.com/spreadsheets/d/1jQ7j-U4Alx8Z10yZS7XghGF6tjReUO_oOUPLgPeQZy4/edit?usp=sharing)
* Planning du quadrimestre
* Modalités de rendu des exercices
* Travail de fin de quadrimestre

--

![](https://cdn.tutsplus.com/webdesign/uploads/legacy/articles/101_history/tutimages/first-web-browser.png)

Le premier navigateur web \(1990\), développé au CERN par [Tim Berners Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee), et utilisé par lui-même et son collègue [Robert Cailliau](https://fr.wikipedia.org/wiki/Robert_Cailliau).

## Au programme

* Internet et World Wide Web, [origines](https://www.youtube.com/watch?v=8LFifvd1Rsk) - [Réseau physique](https://www.submarinecablemap.com/) ou [ceci](https://submarine-cable-map-2018.telegeography.com/)
* Qu'est-ce qu'un site web?
* Navigateurs, serveurs, clients, [adresse IP](https://whatismyipaddress.com/fr/mon-ip), nom de domaine, hébergeurs...
* Les différents langages que nous allons aborder \(HTML, CSS, PHP, javaScript, MySQL\)
* Les langages de balisage \(html, wikimedia, markdown, [SVG](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics)...\) et autres \(programmation\). Aussi jSON.
* Nos outils
* Introduction au [langage HTML](https://www.w3schools.com/html/html_basic.asp) \(le code n'est [jamais très loin](https://www.wikihow.com/View-Source-Code)...\)
* HTML - CSS - JavaScript

## Nos outils

* Le **minimum** : Votre navigateur préféré \(Firefox ou Chrome sont excellents\) + un éditeur de code \([Sublime text](https://www.sublimetext.com/), [Atom](https://atom.io/), [VS code](https://code.visualstudio.com/)\)
* Le **presque indispensable**: l'[inspecteur web](https://developer.mozilla.org/fr/docs/Outils/Inspecteur)
* Eventuellement un éditeur en ligne, type **[Code Pen](https://codepen.io)** ou [jsBin](https://jsbin.com), qui feront office de bac à sable.
* **Netlify**, que nous utiliserons dans un premier temps pour déployer nos pages sur le réseau.
* **FileZilla** que nous utiliserons pour le transfert FTP dans un second temps.

![](https://mdn.mozillademos.org/files/7747/inspector-color-picker.png)

## HTML: Structure et balises

* Principes généraux du langage HTML
* Structure générale
* Doctype
* Balises de **1er niveau** : html, head, body et balises **metas** \(et leur impact sur le SEO notamment\)
* Balises d'**entête** \(title, meta, style, script..\)
* Balises de type **block** : header, footer, h1, h2, ... , ul, ol, li, div, p, blockquote, img, table, form, canvas..
* Balises de type **inline** : span, a, strong, em
* Balises orphelines \(hr, img..\)
* Les [attributs](http://netart.rocks/notes/html)
* Les commentaires

> Listes des [principales balises HTML](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3/memento-des-balises-html)

![](http://netart.rocks/images/html3.gif)Gif: [http://netart.rocks/notes/html](http://netart.rocks/notes/html)

## Ressources

* [**Les origines du World Wide Web**](https://www.youtube.com/watch?v=8LFifvd1Rsk)**, par Robert Cailliau**

Les artistes travaillant avec le web comme matière première s'approprient chaque nouvelle évolution technologique pour l'interroger, la détourner, la redéfinir...

Citons:

1. les **réseaux sociaux**
2. **AI/Machine learning**
3. Le développement d'**extensions** pour les navigateurs
4. Le **hacking**
5. Les **données** \(Big Data\)
6. La **géolocalisation** et les services web associés
7. La **Blockchain**
8. etc.

D'autres artistes, plutôt hors du champ des arts numériques à proprement parler, utilisent certains de ces outils pour des projets spécifiques. Citons **Ai WeiWei** et **Olafur Eliasson** et leur projet [Moon](https://www.moonmoonmoonmoon.com/).

[![](https://d32dm0rphc51dk.cloudfront.net/ecTqQsonsjyPGHIduxNwTg/larger.jpg)](https://www.newrafael.com/notes-on-abstract-browsing/)

**Rafael Rozendaal,** [Abstract browsing](http://www.abstractbrowsing.net/) 17 03 06\(Flickr\), 2017

Ce travail de Rafael Rozendaal, une extension pour le navigateur Chrome, nous donne un indice sur la structure de cette page HTML \(ici, Flickr\). Nous parlerons beaucoup d'**éléments** HTML, souvent de blocs, imbriqués les uns dans les autres \(**BLOCK**\), ou les uns à côté des autres \(**INLINE**\).

## Références

* Jasper Elings
* Jan Robert Leegte, [Untitled mountains](http://www.untitledmountains.com/)
* Cory Arcangel, [Working on My Novel](https://twitter.com/WrknOnMyNovel)
* Melanie Hoff, [Hacking the browser](http://www.artdelicorp.com/category/hacking-the-browser/)
* Rafael Rozendaal, [Manual sequence](http://www.manualsequence.com/)
* Jenny Odell, [Satellite collections](http://www.jennyodell.com/satellite.html)
* [Net.art](https://fr.wikipedia.org/wiki/Net.art)
* Philippe De Jonckheere, [Le désordre](http://www.desordre.net/)
* Jonas Lund, [Critical Mass](https://critical-mass.network/about)
* [http://anthology.rhizome.org/](http://anthology.rhizome.org/)
* Olia Lialina \(Récit, Narration, texte, hypertexte, oeuvre expérience, interactivité\)
* Rafael Rozendaal, [Falling Falling](http://www.fallingfalling.com/), [RRFood](https://twitter.com/rrfood), [Midnight moment](http://www.newrafael.com/times-square-midnight-moment/)
* Les artistes qui utilisent le contexte du web
* Alexei Shulgin, [Form Ar](http://anthology.rhizome.org/form-art)t
* [Jan Robert Leegte](http://www.leegte.org/)

## Autres ressources

* [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet\](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet\)
* [http://nokto.net/media/iut\_2012\_memo-html5.pdf\](http://nokto.net/media/iut_2012_memo-html5.pdf\)
* [http://www.20thingsilearned.com/fr-FR](http://www.20thingsilearned.com/fr-FR)
* [http://www.evolutionoftheweb.com/](http://www.evolutionoftheweb.com/)
* [http://www.camilleroux.com/2008/07/21/histoire-et-avenir-du-web-ebook-pdf/](http://www.camilleroux.com/2008/07/21/histoire-et-avenir-du-web-ebook-pdf/)
* [http://www.w3.org/MarkUp/tims\_editor](http://www.w3.org/MarkUp/tims_editor)
* [http://hakim.se/experiments](http://hakim.se/experiments)
* [http://thecodeplayer.com/](http://thecodeplayer.com/)
* [https://www.bitballoon.com/\](https://www.bitballoon.com/\)
* [http://www.steaw-webdesign.com/css/1-tango.html](http://www.steaw-webdesign.com/css/1-tango.html)
* [http://leaverou.github.com/animatable/](http://leaverou.github.com/animatable/)

---

* Le réseau, l'infrastructure, c'est le café
* L'Internet c'est la politesse \(ce sont les règles, le code de la route, comme le dit Robert Cailliau\)
* L'espace disque chez l'hébergeur, ce sont les frigos \(ou - Les différentes armoires et étagères du bar par exemple\)
* Le serveur c'est le bartender
* Le navigateur c'est le client
* Les adresses IP sont les emplacements des tables, en latitude, longitude et altitude
* Les noms de domaines sont les numéros de tables \(table 5, table 6..\)
* Lever le doigt c'est une requête
* Une page statique, c'est un tonic
* Une page dynamique un cocktail
* Les données de la base, ce sont les ingrédients \(gousse de vaille, glace pillée, menthe, gingembre..\)
* PHP \(par exemple\), la recette à suivre
* Angular est un Américain haché minute \(ou un truc flambé à table..\)
* Et l'eau du robinet est open source, bien sûr...

